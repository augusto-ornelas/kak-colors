Kakoune Colors
-------------

A plug-in for the Kakoune text editor to highlight colors.

== Currently Supported Color Format
- rgb(32, 97, 189), rgba(123, 43, 23, 0.8), #03820F, 0xFFEE00 and kakoune colors rgb:235683

image::alacritty_config_before.png[Alacritty config before]
image::alacritty_config_after.png[Alacritty config after]
image::css_before.png[css file before]
image::css_after.png[css file after]
image::kak_colorscheme.png[kakoune colorscheme]

== Installation

[source, console]
---
$ git clone <this repo>
$ cd kak-colors
$ cargo install --path .
$ cp colors.kak $HOME/.config/kak/autoload/
---

== Configuration

This plug-in provides the command **which-color** which tries to highlight the text representing a color with the correct color. To show all the colors in a buffer you can map a key like:

[source]
---
map global user c '%: which-color<ret>'
---

If you prefer to update the colors after changing them automatically, you can use one of the builtin kakoune hooks, either NormalIdle, InsertIdle or any other that makes more sense for you, to trigger running the command **which-color**.
