#[derive(Debug, PartialEq)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

use std::fmt;

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:02X}{:02X}{:02X}", self.r, self.g, self.b)
    }
}

impl Color {
    pub fn best_contrast(&self) -> Self {
        if self.luminance() > 0.179 {
            Color::black()
        } else {
            Color::white()
        }
    }

    pub fn black() -> Self {
        Self { r: 0, g: 0, b: 0 }
    }

    fn from_hex(s: &str) -> Self {
        let r = u8::from_str_radix(&s[..2], 16).unwrap();
        let g = u8::from_str_radix(&s[2..4], 16).unwrap();
        let b = u8::from_str_radix(&s[4..], 16).unwrap();
        Color { r, g, b }
    }

    pub fn luminance(&self) -> f64 {
        let srgb = self.to_srgb();
        let lum = srgb
            .iter()
            .map(|&c| {
                if c <= 0.03928 {
                    c / 12.92
                } else {
                    ((c + 0.055) / 1.055).powf(2.4)
                }
            })
            .collect::<Vec<f64>>();
        lum.get(0).unwrap() * 0.2126 + lum.get(1).unwrap() * 0.7152 + lum.get(2).unwrap() * 0.0722
    }

    pub fn to_srgb(&self) -> [f64; 3] {
        [
            self.r as f64 / 255.0,
            self.g as f64 / 255.0,
            self.b as f64 / 255.0,
        ]
    }

    pub fn white() -> Self {
        Self {
            r: 0xff,
            g: 0xff,
            b: 0xff,
        }
    }
}

use std::str::FromStr;
use u8;
// Warning: alpha values are ignored
impl FromStr for Color {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if (s.starts_with("rgba(") || s.starts_with("rgb(")) && s.ends_with(")") {
            let s = if s.strip_prefix("rgb(").is_some() {
                s.strip_prefix("rgb(").unwrap()
            } else {
                s.strip_prefix("rgba(").unwrap()
            };
            let s = s.strip_suffix(")").unwrap();
            let values: Vec<&str> = s.split(',').map(|value| value.trim()).collect();
            let values: Vec<u8> = values
                .iter()
                .map(|val| val.parse::<u8>().unwrap_or_default())
                .collect();
            Ok(Color {
                r: *values.get(0).unwrap(),
                g: *values.get(1).unwrap(),
                b: *values.get(2).unwrap(),
            })
        } else if s.starts_with("#") {
            let s = s.strip_prefix("#").unwrap();
            Ok(Color::from_hex(s))
        } else if s.starts_with("0x") {
            let s = s.strip_prefix("0x").unwrap();
            Ok(Color::from_hex(s))
        } else if s.starts_with("rgb:") {
            let s = s.strip_prefix("rgb:").unwrap();
            Ok(Color::from_hex(s))
        } else {
            Err(String::from("Not a valid rgb string"))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn given_a_line_return_all_colors() {
        let colors =
            extract_posible_colors("rgb(10, 10, 10) more here 0x443388 rgb:668822".to_string());
        assert_eq!(
            Some(vec![
                "rgb(10, 10, 10)".to_string(),
                "0x443388".to_string(),
                "rgb:668822".to_string()
            ]),
            colors
        );
    }

    #[test]
    fn test_rgba() {
        let color: Color = "rgba(40, 40, 40, 0.8)".parse().unwrap();
        assert_eq!(
            color,
            Color {
                r: 40,
                g: 40,
                b: 40
            }
        )
    }

    #[test]
    fn test_kakoune_color_parsing() {
        let color = "rgb:0033ff".parse::<Color>().unwrap();
        assert_eq!(
            color,
            Color {
                r: 0,
                g: 0x33,
                b: 0xff,
            }
        )
    }

    #[test]
    fn test_with_hex() {
        let color = "#ff00cc".parse::<Color>().unwrap();
        assert_eq!(
            color,
            Color {
                r: 0xff,
                g: 0,
                b: 0xcc,
            }
        )
    }

    #[test]
    fn test_parsing_from_rgb() {
        let color = "rgb(123, 33, 23)".parse::<Color>().unwrap();
        assert_eq!(
            color,
            Color {
                r: 123,
                g: 33,
                b: 23
            }
        );
        let color = "rgb(123,33,23)".parse::<Color>().unwrap();
        assert_eq!(
            color,
            Color {
                r: 123,
                g: 33,
                b: 23
            }
        )
    }

    #[test]
    fn test_best_contrast_with_red() {
        //for ff0022 the best contrast is with black
        assert_eq!(
            Color {
                r: 0xff,
                g: 0x00,
                b: 0x22
            }
            .best_contrast(),
            Color { r: 0, g: 0, b: 0 }
        )
    }

    #[test]
    fn test_best_contrast_with_light_blue() {
        //for 69B5AE the best contrast is with black
        assert_eq!(
            Color {
                r: 0x69,
                g: 0xB5,
                b: 0xAE
            }
            .best_contrast(),
            Color { r: 0, g: 0, b: 0 }
        )
    }

    #[test]
    fn test_best_contrast_with_white() {
        //for white the best contrast is with black
        assert_eq!(
            Color {
                r: 0xff,
                g: 0xff,
                b: 0xff
            }
            .best_contrast(),
            Color { r: 0, g: 0, b: 0 }
        )
    }

    #[test]
    fn test_best_contrast_with_very_light_blue() {
        //for 99CDC8 the best contrast is with black
        assert_eq!(
            Color {
                r: 0x99,
                g: 0xCD,
                b: 0xC8
            }
            .best_contrast(),
            Color { r: 0, g: 0, b: 0 }
        )
    }
}

use lazy_static::lazy_static;
use regex::Regex;

fn extract_posible_colors(s: String) -> Option<Vec<String>> {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r"((rgb:|0x|#)[a-fA-F0-9]{6}|rgba:[a-fA-F0-9]{8}|(rgba|rgb)\([^\)]+\))")
                .unwrap();
    }
    let found: Vec<String> = RE.find_iter(&s).map(|x| x.as_str().to_string()).collect();
    if found.len() > 0 {
        return Some(found);
    }
    None
}

use std::io::{self, BufRead};

fn main() {
    io::stdin()
        .lock()
        .lines()
        .filter_map(|x| x.ok())
        .filter_map(|x| extract_posible_colors(x))
        .for_each(|x| {
            x.iter().for_each(|y| {
                // TODO: handle when the Result
                // This code assumes the string is a valid colors
                let color = y.parse::<Color>().unwrap();
                let hlid = y.replace(' ', "_");
                println!(
                    "evaluate-commands %{{ try %{{ add-highlighter window/colors/{} regex \"\\Q{}\\E\" 0:rgb:{},rgb:{} }} }}",
                    hlid,
                    y,
                    color.best_contrast(),
                    color,
                )
            })
        });
}
