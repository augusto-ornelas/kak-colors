# This changes the background color to the color represented by the string
define-command -docstring 'Highlight the hex color with itself' which-color %{
  try %{
    add-highlighter window/colors group
  }
  evaluate-commands %sh{
    printf '%s\n' "$kak_selection" | kak_colors
  }
}
